Стайлгайд в этом курсе основан на [гугловом](https://google.github.io/styleguide/cppguide.html) стайлгайде плюсов, за редкими (или нет) исключениями. Два больших исключения:
* отступы 4 пробела, а не 2
* расширением файлов с кодом может быть .cpp, не только .cc

Что обязательно в лабе третьего семинара:
* весь [нейминг](https://google.github.io/styleguide/cppguide.html#Naming) (исключая нейминг файлов)
* [контроль доступа](https://google.github.io/styleguide/cppguide.html#Access_Control) и [порядок объявления](https://google.github.io/styleguide/cppguide.html#Declaration_Order) 
* [условия](https://google.github.io/styleguide/cppguide.html#Conditionals) и [циклы](https://google.github.io/styleguide/cppguide.html#Conditionals). Кроме абзаца For historical reasons, we allow one excpetion... и дальше про случаи, когда тело if/for можно не окружать {} и не переносить на отдельную строку. Вот это нельзя: тело if/for всегда пишется с новой строки и окружено фигурными скобками
* [приведение типов](https://google.github.io/styleguide/cppguide.html#Casting) (в большинстве случаев вы хотите переходить сразу ко второму пункту, static_cast)
* [++i over i++](https://google.github.io/styleguide/cppguide.html#Preincrement_and_Predecrement)

Где-то дальше будет...
* [const](https://google.github.io/styleguide/cppguide.html#Preincrement_and_Predecrement): следует использовать const везде, где данные не должны меняться. В том числе, классы, являющиеся структурами данных, не должны менять свои входные данные
* [integers](https://google.github.io/styleguide/cppguide.html#Integer_Types) -- не используйте беззнаковые типы, если задача не создана под них
* tbc
